# -*- coding: utf-8 -*-
# Copyright (c) 2018, Devienta and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from frappe.model.document import Document

class ServiceTeam(Document):
	pass

@frappe.whitelist()
def get_team_members(team):
	team_doc = frappe.get_doc('Service Team', team)
	servants_names = []
	for servant in team_doc.servants:
		if servant.is_active:  servants_names.append(servant.servant_name_ar)
	return servants_names
