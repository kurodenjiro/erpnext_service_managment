// Copyright (c) 2019, Devienta and contributors
// For license information, please see license.txt

frappe.ui.form.on("Conference Reservation Form", {
  onload: function (frm) {
    // TODO: Figure out a better way to push the parameter instead of doing it manually.
    let conference_parameter = (frappe.route_options && frappe.route_options.conference) ? frappe.route_options.conference : '';
    if (conference_parameter) {
      frm.set_value('conference', conference_parameter);
    }
  },
  validate: function (frm) {
    let total_fees = 0;
    $.each(frm.doc['conf_members'] || [], function (i, member) {
      total_fees += member.fees;
    });
    frm.set_value('total_fees', total_fees);
  }

});