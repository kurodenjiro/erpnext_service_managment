# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"label": _("Servants"),
			"items": [
				{
					"type": "doctype",
					"name": "Servant",
					"label": _("Servant")
				},
				{
					"type": "doctype",
					"name": "Servant Relative",
					"label": _("Servant Relative")
				},
				{
					"type": "doctype",
					"name": "Service Team",
					"label": _("Service Team")
				},
				{
					"type": "doctype",
					"name": "Service Area",
					"label": _("Service Area")
				}
			]
		},
		{
			"label": _("Oraganizational Hierarchy"),
			"items": [
				{
					"type": "doctype",
					"name": "Eparchy",
					"label": _("Eparchy")
				},
				{
					"type": "doctype",
					"name": "Service Advisor",
					"label": _("Service Advisor")
				},
				{
					"type": "doctype",
					"name": "Governorate",
					"label": _("Governorate")
				},
				{
					"type": "doctype",
					"name": "Church",
					"label": _("Church")
				},
				{
					"type": "doctype",
					"name": "Priest",
					"label": _("Priest")
				}
			]
		},
		{
			"label": _("Conferences"),
			"items": [
				{
					"type": "doctype",
					"name": "Conference",
					"label": _("Conference")
				}
			]
		}
	]
